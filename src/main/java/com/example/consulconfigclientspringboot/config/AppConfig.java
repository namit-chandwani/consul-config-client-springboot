package com.example.consulconfigclientspringboot.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

@Component
@RefreshScope
@Data
public class AppConfig {

    @Value("${name}")
    private String name;

    @Value("${age}")
    private int age;

    @Value("${city}")
    private String city;

}
