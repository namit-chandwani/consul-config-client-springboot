package com.example.consulconfigclientspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsulConfigClientSpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsulConfigClientSpringbootApplication.class, args);
    }

}
