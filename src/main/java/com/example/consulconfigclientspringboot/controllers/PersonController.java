package com.example.consulconfigclientspringboot.controllers;

import com.example.consulconfigclientspringboot.config.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/person")
public class PersonController {

    @Autowired
    private AppConfig appConfig;

    @GetMapping("/details")
    public String getPersonDetails(){
        return appConfig.toString();
    }
}
